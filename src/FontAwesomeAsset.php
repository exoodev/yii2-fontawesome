<?php

namespace exoo\fontawesome;

/**
 * Font Awesome asset bundle.
 */
class FontAwesomeAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/font-awesome';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/all.min.css',
    ];
}
